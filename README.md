This repository is used for storing data for WissKI migration. 

Attachments for issues have been removed during transition from gitlab.cs! New storage is at https://faubox.rrze.uni-erlangen.de/getlink/fiBR6SDyg1Yh671CaDro3TUp/attachments-gitlab.cs.wisski
