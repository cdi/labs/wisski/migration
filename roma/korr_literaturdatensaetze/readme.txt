Literaturdatensätze in Roma korrigieren
04.10.22

Nach dem Einrichten des Zotero-Adapters sind die händisch angelegten und verlinkten Literaturdatensätze obsolet geworden. 
Diese müssen an den entsprechenden Stellen durch die von Zotero reingespiegelten Datenästze ersetzt werden und am Ende herausgelöscht werden. 
Am Ende der Maßnahme muss noch eingestellt werden, dass in WissKI keine Literaturdatensätze angelegt werden dürfen. 
Sie werden nur in Zotero verwaltet und können im WissKI referenziert werden. 

-> manage form display "add only existing entities"
-> title pattern korrigieren, sodass keine Felder der alten Literaturmaske verwendet werden


Ersetzen der Literatureinträge in Roma
11 betroffene Felder: 

Bibliographie Bauwerk
Bibliographie Wandmalerei

Datierungszuschreibung von Bau / Bauwerk
Datierungszuschreibung von Stiftung / Bauwerk

Datierungszuschreibung von Bau / Raumkompartiment
Datierungszuschreibung von Bau / Subkompartiment
Datierungszuschreibung von Bau / Wand 
Datierungszuschreibung von Wandmalerei
Künstlerzuschreibung von Wandmalerei

Datierungszuschreibung von Ereignis
Datierungszuschreibung von Bau / Gesamtanlage


Erste query in SPARQL
Ersetzt alle Felder, in denen (mit P106i_forms_part_of ->) der Inhalt Dehio 2008 alt stand, mit dem neuen Inhalt Dehio 2008 (Zotero)

PREFIX wm: <http://va.gnm.de/roma/ontologie/>
PREFIX ecrm: <http://erlangen-crm.org/160714/>
DELETE { 
    GRAPH ?g { ?part <http://erlangen-crm.org/160714/P106i_forms_part_of> <https://roma.wisski.data.fau.de/content/59fc532bd783c> }.

  }
  INSERT {
    GRAPH ?g { ?part <http://erlangen-crm.org/160714/P106i_forms_part_of> <https://www.zotero.org/groups/4727128/items/itemKey/AUNDDR8M> } .
  }
 where {
    GRAPH ?g { ?part a wm:sub31_Part_of_Secondary_Source_Document . 
    ?part <http://erlangen-crm.org/160714/P106i_forms_part_of> <https://roma.wisski.data.fau.de/content/59fc532bd783c>}
  }